/**
 * Configuration module.
 * @module
 */

var _           = require('lodash'),
    bunyan     = require('bunyan'),
    fs          = require('fs'),
    path        = require('path'),
    session     = require('express-session'),
    SQLiteStore = require('connect-sqlite3')(session);

/**
 * The configuration data.
 * @type{Object}
 */
var config = {
  /**
   * Settings for the server.
   * @type {Object}
   * @prop int defaultPort Default port to start the server on if none given in the command line.
   */
  server: {
    defaultPort: 3002,
    urlPrefix:   'http://localhost:3002' // Specify how to publicly access the key server.
  },

  /**
   * Whitelisted domains allowed to use CORS to access /api endpoints.
   * @type {Array}
   */
  corsDomains: [],

  /**
   * Details about URLs.
   * @type {Object}
   * @prop {string} landingUrl The URL for the websites landing page.
   * @prop {string} homeUrl The home URL for authenticated users.
   * @prop {string} loginUrl A page to redirect users when they need to login.
   */
  urls: {
    landingUrl:     '/',
    landingPageUrl: '/landing',
    homeUrl:        '/home',
    loginUrl:       '/login'
  },

  /**
   * Settings passed to jade templates.
   * @type {Object}
   * @prop {string} title Title of the page.
   * @prop {string} getParams Function to add new params to the existing params.
   */
  jadeLocals: {
    title:    'MessageGuard Key Server',
    siteName: 'MessageGuard Key Server'
  },

  /**
   * Settings that are passed to the session on creation.
   * @type {Object}
   */
  session: {
    secret:            'secretValue',
    resave:            true,
    saveUninitialized: true,

    cookie: {maxAge: 7 * 24 * 60 * 60 * 1000}, // 1 week.
    store:  new SQLiteStore
  },

  /**
   * Settings passed to the sequelize constructor.
   * @type {Object}
   */
  db: {
    database: 'MessageGuard-KeyServer',
    username: 'ISRL',
    password: 'ISRL',

    options: {
      host:    'localhost',
      dialect: 'sqlite',

      logging: false, // Disable logging to the console.

      pool: {
        max:  5,
        min:  0,
        idle: 10000
      },

      storage: './local.db'
    }
  },

  /**
   * Settings for the logger.
   * @type {Object}
   * @prop {string} format Format of the logger.
   * @prop {string} options Options to pass to the logger.
   */
  logger: {
    format:  'dev',
    options: {}
  },

  /**
   * Settings for users.
   * @type {Object}
   * @prop {RegExp} usernameRegex Regex for usernames.
   * @prop {RegExp} passwordRegex Regex for passwords.
   */
  users: {
    usernameRegex: /^[!-~]{1,255}$/,
    emailRegex:    /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/,
    passwordRegex: /^[!-~ ]{8,255}$/
  },

  /**
   * Settings for identities.
   * @type {Object}
   * @prop {string} messageGuardType Type of the MessageGuard identities.
   * @prop {RegExp} nameRegex Regex that names must match.
   */
  identities: {
    messageGuardType: 'MessageGuard',

    /**
     * Setting to pass to the passport-facebook strategy.
     * @type {Object}
     * @prop {Object} strategyOptions Options for the strategy.
     * @prop {Object} requestOptions Options for the request.
     */
    facebook: {
      strategyOptions: {
        callbackURL:       "/identity/add/facebook/callback",
        enableProof:       true,
        passReqToCallback: true
      },

      requestOptions: {
        authType: 'reauthenticate'
      }
    },

    /**
     * Settings to use for email authentication.
     * @type {Object}
     */
    email: {
      sawTimeout: 5 * 60 * 1000, // 5 minutes.
      sawLength:  255,

      subject:         'Validate your email address',
      defaultEndpoint: '/identity/add/email/callback',
      emailBodyFormat: '<h1>Welcome to MessageGuard!</h1>' +
                       '<h2>Click the following link to validate your email address:</h2>' +
                       '<h2><a href="%s%s?sawShare=%s">Verify email</a></h2>',

      transportOptions: {
        service: "gmail",
        host: "smtp.gmail.com",
        auth: {
          user: "",
          pass: ""
        },
        logger: bunyan.createLogger({name: 'nodemailer'})
      }
    }
  },

  /**
   * Settings for schemes.
   * @type {Object}
   * @prop
   */
  schemes: {

    /**
     * Params for IBE.
     * @type {Object}
     * @prop {string} secret The secret value for the IBE server.
     */
    ibe: {
      secret: '1058120784561923413943181891454816348057296185745677105212341085343'
    }
  }
};

// If deploy-config exists, merge its settings with these settings.
var deployConfigAvailable = false;
try {
  fs.accessSync(path.join(__dirname, './deploy-config.js'), fs.F_OK);
  deployConfigAvailable = true;
} catch (e) { }

if (deployConfigAvailable) {
  _.merge(config, require('./deploy-config'));
}

module.exports = config;