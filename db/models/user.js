/**
 * Model for users.
 * @module
 */

var bCrypt    = require('bcrypt-nodejs'),
    Sequelize = require('sequelize');

var config = require('../../config'),
    db     = require('../db');

/**
 * The user data model.
 * @type {Model}
 */
var User = db.define('User', {
  username: {
    type:      Sequelize.STRING,
    allowNull: false,
    validate:  {is: config.users.usernameRegex}
  },
  password: {
    type:      Sequelize.STRING,
    allowNull: false
  },
}, {
  indexes:      [
    {
      name:   'UniqueUsername',
      unique: true,
      fields: [
        Sequelize.fn('lower', Sequelize.col('username')),
      ]
    }
  ],
  classMethods: {
    /**
     * Find the user by their name.
     * @param {string} username User's name.
     * @param {Object?} options Options to pass to the method.
     * @returns {Promise} Promise that will return the user.
     */
    findByName: function (username, options) {
      return User.findOne({
        where: db.where(
            db.fn('lower', db.col('username')),
            db.fn('lower', username))
      }, options);
    },

    /**
     * Create a user with the given username and password.
     * @param {string} username User's name.
     * @param {string} password User's password.
     * @param {Object?} options Options to pass to the method.
     * @returns {Promise} Promise that will return the created user.
     */
    createWithPassword: function (username, password, options) {
      return User.create({
        username: username,
        password: bCrypt.hashSync(password, bCrypt.genSaltSync(10))
      }, options);
    },

    /**
     * Find the user with the given username and password.
     * @param {string} username User's name.
     * @param {string} password User's password.
     * @param {Object?} options Options to pass to the method.
     * @returns {Promise} Promise that will return the user.
     */
    findWithPassword: function (username, password, options) {
      return User.findByName(username, options).then(function (user) {
        if (!user) {
          return null;
        } else if (!bCrypt.compareSync(password, user.password)) {
          throw new Error('Incorrect password');
        } else {
          return user;
        }
      });
    }
  }
});

module.exports = User;