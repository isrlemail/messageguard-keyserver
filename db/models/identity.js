/**
 * Model for identity.
 * @module
 */

var Sequelize = require('sequelize');

var config = require('../../config'),
    db     = require('../db'),
    User   = require('./user');

/**
 * The identity data model.
 * @type {Model}
 */
var Identity = db.define('Identity', {
  type:        {
    type:      Sequelize.STRING,
    allowNull: false
  },
  value:       {
    type:      Sequelize.STRING,
    allowNull: false
  },
  displayName: {
    type:      Sequelize.STRING,
    allowNull: false,
    validate:  {len: [1, 255]}
  },
  data:        {
    type:      Sequelize.TEXT,
    allowNull: true
  }
}, {
  indexes:         [
    {
      name:   'UniqueTypeAndValue',
      unique: true,
      fields: [
        Sequelize.fn('lower', Sequelize.col('type')),
        Sequelize.fn('lower', Sequelize.col('value'))
      ]
    }
  ], classMethods: {
    /**
     * Find an identity.
     * @param {string} value Name of the identity.
     * @param {string} type Type of the identity.
     * @param {Object?} options Options to pass to the method.
     * @returns {Promise} Promise that returns the identity.
     */
    findIdentity: function (type, value, options) {
      return Identity.findOne({
        where: db.and(
            db.where(
                db.fn('lower', db.col('type')),
                db.fn('lower', type)),
            db.where(
                db.fn('lower', db.col('value')),
                db.fn('lower', value))
        )
      }, options);
    },

    /**
     * Create an identity for the given user.
     * @param {Instance} user User that will own the identity.
     * @param {Object} data Data for the identity.
     * @param {Object} options Options to pass when creating the identity.
     * @returns {Promise.<this|Errors.ValidationError>}
     */
    createIdentity: function (user, data, options) {
      var identity = Identity.build(data);
      identity.setUser(user, {save: false});
      return identity.save(options);
    }
  }
});

// Set up associations.
Identity.belongsTo(User, {foreignKey: {allowNull: false}, onDelete: 'CASCADE'});
User.hasMany(Identity, {
  as: 'identities'
});

/*
// Automatically create an identity for each user.
User.hook('afterCreate', function (user, options) {
  return Identity.createIdentity(user, {
    type:        config.identities.messageGuardType,
    value:       user.username,
    displayName: user.username
  }, {transaction: options.transaction});
});
*/

module.exports = Identity;