/**
 * Model for pgp key storage.
 * @module
 */

var Sequelize = require('sequelize');

var config   = require('../../../config'),
    db       = require('../../db'),
    Identity = require('../identity');

/**
 * The PGPKey data model.
 * @type {Model}
 */
var PGPKey = db.define('PGPKey', {
  isPublic: {
    type:      Sequelize.BOOLEAN,
    allowNull: false
  },
  contents: {
    type:      Sequelize.TEXT,
    allowNull: false
  },
}, {
  indexes:         [
    {
      name:   'UniqueIdentityAndIsPublic',
      unique: true,
      fields: ['IdentityId', 'isPublic']
    }
  ], classMethods: {
    /**
     * Create an PGP key for the given identity.
     * @param {Instance} identity Identity that will own the PGP key.
     * @param {Object} data Data for the PGP key.
     * @param {Object} options Options to pass when creating the PGP key.
     * @returns {Promise.<this|Errors.ValidationError>}
     */
    createPGPKey: function (identity, data, options) {
      var key = PGPKey.build(data);
      key.setIdentity(identity, {save: false});
      return key.save(options);
    }
  }
});

// Setup associations.
PGPKey.belongsTo(Identity, {foreignKey: {name: 'IdentityId', allowNull: false}, onDelete: 'CASCADE'});
Identity.hasMany(PGPKey);

module.exports = PGPKey;