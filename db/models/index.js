/**
 * Module that loads all of the models.
 * @module
 */

var requireDir = require('require-dir');

requireDir('.', {recurse: true});
module.exports = require('../db').models;