/**
 * Functions for accessing the database.
 * @module
 * @requires config
 */

var Sequelize = require('sequelize');

var config = require('../config');

module.exports = new Sequelize(config.db.database, config.db.username, config.db.password, config.db.options);

// Make sure all the models are loaded. This is neccessary for correct operation.
require('./models');
