/**
 * Route handler for the account pages`.
 * @module
 */

var express = require('express');

var config   = require('../config'),
    identity = require('./identity');

var router = express.Router();

// GET home page.
router.get('/', function (req, res, next) {
  req.user.getIdentities().then(function (identities) {
    res.render('home', {
      username:         req.user.username,
      identities:       identities,
      identityHandlers: identity.identityHandlers
    });
  });
});

module.exports = router;
