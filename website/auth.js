/**
 * Route handler for authentication pages.
 * @module
 */

var express       = require('express'),
    LocalStrategy = require('passport-local').Strategy,
    passport      = require('passport');

var config = require('../config'),
    db     = require('../db/db'),
    User   = db.models.User;

var router = express.Router();

// Setup user serialization for passport.
passport.serializeUser(function (user, done) {
  done(null, user.id);
});

passport.deserializeUser(function (id, done) {
  User.findById(id).then(function (user) {
    done(null, user);
  }).catch(function (e) {
    done(e);
  });
});

/**
 * Redirect to the desired URL after authentication.
 */
var redirectWhenAuthenticated = function (req, res, next) {
  if (req.isAuthenticated()) {
    req.flash('auth-username');
    var desiredUrl = req.session.desiredUrl || config.urls.landingUrl;
    delete req.session.desiredUrl;
    res.redirect(desiredUrl);
  } else {
    next();
  }
};

/// --------------------------------------------------------------------------------------------------------------------
/// Login
/// --------------------------------------------------------------------------------------------------------------------

// GET the login page.
router.get(config.urls.loginUrl, redirectWhenAuthenticated, function (req, res, next) {
  res.render('login', {
    username: req.flash('auth-username'),
    errors:   req.flash('error')
  });
});

// Passport hook to login users.
passport.use('login', new LocalStrategy(
    {passReqToCallback: true},
    function (req, username, password, done) {
      User.findWithPassword(username, password).then(function (user) {
        if (user) {
          done(null, user);
        } else {
          done(null, false, {message: 'Bad username'});
        }
      }).catch(db.Error, function (e) {
        done(e);
      }).catch(function (e) { // Catches password mismatch.
        done(null, false, {message: e.message});
      });
    }));

// Login the user.
router.post(config.urls.loginUrl, function (req, res, next) {
  // Store the username so it doesn't have to be retyped.
  req.flash('auth-username');
  req.flash('auth-username', req.body.username);

  // Validate the request.
  req.checkBody('username', 'Invalid username').matches(config.users.usernameRegex);
  req.checkBody('password', 'Passwords must be at least 8 characters long').matches(config.users.passwordRegex);

  var errors = req.validationErrors();
  if (errors) {
    errors.forEach(function (error) {req.flash('error', error.msg)});
    res.redirect('login');
  } else {
    next();
  }
}, passport.authenticate('login', {
  failureRedirect: config.urls.loginUrl,
  failureFlash:    true
}), redirectWhenAuthenticated);

/// --------------------------------------------------------------------------------------------------------------------
/// Signup
/// --------------------------------------------------------------------------------------------------------------------

// Handle the registration page.
router.use('/signup', require('./signup'));

// Handle Logout
router.get('/logout', function (req, res, next) {
  req.logout();
  res.redirect('/');
});

module.exports = router;
