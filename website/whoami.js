/**
 * Route handler for authentication pages.
 * @module
 */

var express       = require('express'),
    LocalStrategy = require('passport-local').Strategy,
    passport      = require('passport');

var config     = require('../config'),
    db         = require('../db/db'),
    Identity   = db.models.Identity;

var router = express.Router();

function whoami(req, res, next) {
  if (!req.isAuthenticated()) {
    res.send({
      err: 'not_authenticated'
    });

    return;
  }

  req.user.getIdentities().then(function(identities) {
    res.send({
      username: req.user.username,
      identities: identities.map(function(i) {
        return {
          type: i.get('type'),
          value: i.get('value')
        };
      })
    });
  }, next);
}

router.get('/', whoami);
router.post('/', whoami);

module.exports = router;
