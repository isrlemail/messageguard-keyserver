/**
 * Route handler for the index page.
 * @module
 */

var express = require('express');

var config = require('../config');

var router = express.Router();

// GET the index page.
router.get(config.urls.landingUrl, function (req, res, next) {
  if (req.isAuthenticated()) {
    res.redirect(config.urls.homeUrl);
  } else {
    //res.render('index');
    res.redirect(config.urls.landingPageUrl);
  }
});

module.exports = router;
