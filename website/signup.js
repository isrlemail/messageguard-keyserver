/**
 * Route handler for authentication pages.
 * @module
 */

var express       = require('express'),
    LocalStrategy = require('passport-local').Strategy,
    passport      = require('passport');

var config = require('../config'),
    db     = require('../db/db'),
    User   = db.models.User;

var router = express.Router();

// Setup user serialization for passport.
passport.serializeUser(function (user, done) {
  done(null, user.id);
});

passport.deserializeUser(function (id, done) {
  User.findById(id).then(function (user) {
    done(null, user);
  }).catch(function (e) {
    done(e);
  });
});

/**
 * Redirect to the desired URL after authentication.
 */
var redirectWhenAuthenticated = function (req, res, next) {
  if (req.isAuthenticated()) {
    req.flash('auth-username');
    var desiredUrl = req.session.desiredUrl || config.urls.landingUrl;
    delete req.session.desiredUrl;
    res.redirect(desiredUrl);
  } else {
    next();
  }
};

/// --------------------------------------------------------------------------------------------------------------------
/// Signup
/// --------------------------------------------------------------------------------------------------------------------

// GET the signup page.
router.get('/', redirectWhenAuthenticated, function (req, res, next) {
  var errors = req.flash('error');
  res.render('signup/index', {
    username: req.flash('auth-username'),
    errors:   errors
  });
});

// Passport hook to register a user.
passport.use('signup', new LocalStrategy(
    {passReqToCallback: true},
    function (req, username, password, done) {
      User.createWithPassword(username, password).then(function (user) {
        done(null, user);
      }).catch(db.UniqueConstraintError, function (e) {
        done(null, false, {message: 'Username already taken'});
      }).catch(function (e) {
        done(e);
      });
    }));

// Handle Registration POST
router.post('/', function (req, res, next) {
  // Store the username so it doesn't have to be retyped.
  req.flash('auth-username');
  req.flash('auth-username', req.body.username);

  // Validate the request.
  req.checkBody('username', 'Invalid username').matches(config.users.usernameRegex);
  req.checkBody('username', 'Invalid email address').isEmail();
  req.checkBody('password', 'Passwords must be at least 8 characters long').matches(config.users.passwordRegex);
  req.checkBody('password2', 'The passwords do not match').equals(req.body.password);

  var errors = req.validationErrors();
  if (errors) {
    res.status(400).send({
      errors: errors.map(function (e) { return e.msg; })
    });
  } else {
    next();
  }
}, passport.authenticate('signup', {
  failureRedirect: '/signup',
  failureFlash:    true
}), function (req, res, next) {
  req.session.validate_email_from_signup = req.body.username;
  req.session.desiredUrl = '/identity/add/email';

  next();
}, redirectWhenAuthenticated);

router.get('/verified', function (req, res, next) {
  res.render('signup/post-verify');
});

module.exports = router;
