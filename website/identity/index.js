/**
 * Route handler for authentication pages.
 * @module
 */

var express    = require('express'),
    requireDir = require('require-dir'),
    util       = require('util');

var config   = require('../../config'),
    db       = require('../../db'),
    Identity = db.models.Identity,
    WebError = require('../../web-error');

var router = express.Router();

/**
 * Delete the given identity.
 */
router.post('/delete', function (req, res, next) {
  // Validate the request.
  req.checkBody('id', 'id is required').isNumeric();
  var errors = req.validationErrors();
  if (errors) {
    next(new WebError(400, util.inspect(errors)));
    return;
  }

  // Get the appropriate ID and delete it. Then redirect to the home page.
  Identity.findById(req.body.id).then(function (identity) {
    if (!identity) {
      next(new WebError(400, 'Invalid identity'));
    }

    if (identity.type === config.identities.messageGuardType) {
      next(new WebError(400, "Can't delete MessageGuard identities"));
      return;
    }

    if (identity[Identity.associations.User.foreignKey] !== req.user[Identity.associations.User.targetKey]) {
      next(new WebError(403, "User doesn't own the requested identity"));
      return;
    }

    return identity.destroy().then(function () {
      res.redirect(req.get('Referrer'));
    });
  }).catch(function (e) {
    next(e);
  });
});

/**
 * Add an identity.
 */
router.post('/add', function (req, res, next) {
  req.flash('identity-add-referrer');
  req.flash('identity-add-referrer', req.get('Referrer'));
  res.redirect('add/' + req.body.type);
});

// Register handlers for adding identities.
var hash = requireDir('.', {recurse: true});
router.identityHandlers = [];

Object.keys(hash).forEach(function (name) {
  var identityRouter = hash[name].router;
  router.use('/add/' + identityRouter.routeName, identityRouter);
  router.identityHandlers.push(identityRouter);
});

module.exports = router;
