/**
 * Route handler for adding facebook identity.
 * @module
 */

var crypto     = require('crypto'),
    express    = require('express'),
    nodemailer = require('nodemailer'),
    util       = require('util');

var config   = require('../../config'),
    db       = require('../../db'),
    Identity = db.models.Identity;

var router = express.Router();
router.routeName = 'email';
router.displayName = 'Email';

var sawLength = config.identities.email.sawLength;

// Setup email.
var transporter = nodemailer.createTransport(config.identities.email.transportOptions);

var startSaw = function (email, req, res, success, err) {
  // Generate a SAW key.
  crypto.randomBytes(sawLength * 2, function (e, data) {
    if (e) {
      err(e);
      return;
    }

    // Get the key and the share.
    var key    = data.slice(0, sawLength),
        share1 = data.slice(sawLength),
        share2 = new Buffer(sawLength);
    for (var i = 0; i < sawLength; i++) {
      share2[i] = key[i] ^ share1[i];
    }

    transporter.sendMail({
      from:    config.identities.email.from,
      to:      email,
      subject: config.identities.email.subject,
      html:    util.format(config.identities.email.emailBodyFormat, config.server.urlPrefix,
          config.identities.email.defaultEndpoint,
          encodeURIComponent(share2.toString('base64')))
    }, function(e) {
      if (e) {
        err(e);
        return
      }

      // The key is stored locally on the server (i.e., session data). The first share is sent in a cookie, and the second
      // share is sent over email.
      req.session.saw = {email: email, key: key.toString('base64')};
      res.cookie('sawShare', share1.toString('base64'), {maxAge: config.identities.email.sawTimeout, httpOnly: true});

      success();
    });
  });
};

var completeSaw = function (user, req, res, success, err) {
  var key    = req.session.saw.key,
      share1 = req.cookies.sawShare,
      share2 = req.query.sawShare;

  if (!key || !share1 || !share2) {
    err('You are missing some data. Make sure you click the link on the same device you are trying to log into.');
    return;
  }

  key = new Buffer(key, 'base64');
  share1 = new Buffer(share1, 'base64');
  share2 = new Buffer(share2, 'base64');

  if (key.length != sawLength || share1.length != sawLength || share2.length != sawLength) {
    err('Validation data was currupted: ' + key.length + ', ' + share1.length + ', ' + share2.length);
    return;
  }

  // Check the saw key.
  var result = new Buffer(sawLength);
  for (var i = 0; i < sawLength; i++) {
    result[i] = share1[i] ^ share2[i];
  }

  if (!result.equals(key)) {
    err('Incorrect link');
    return;
  }

  // Create the identity.
  var email = req.session.saw.email;

  Identity.createIdentity(user, {
    type:        'Email',
    value:       email,
    displayName: email
  }).then(function (identity) {
    delete req.session.saw;
    res.clearCookie('sawShare');

    success();
  }).catch(db.UniqueConstraintError, function (e) {
    err('Identity is already owned by another user');
  }).catch(function (e) {
    err(e.message);
  });
};

// Enter the email address that SAW will verify.
router.get('/', function (req, res, next) {
  if (req.session.validate_email_from_signup) {
    var email = req.session.validate_email_from_signup;
    delete req.session.validate_email_from_signup;

    startSaw(email, req, res, function () {
      req.flash('identity-add-referrer');
      req.flash('identity-add-referrer', '/signup/verified');

      res.render('signup/pre-verify', {
        email: email
      });
    }, function (sawError) {
      next(sawError);
    });
  } else {
    res.render('identity/email');
  }
});

// Start a SAW request.
router.post('/', function (req, res, next) {
  // Validate the request.
  req.checkBody('email', 'Invalid email address').isEmail();

  var errors = req.validationErrors();
  if (errors) {
    res.render('identity/email', {
      email:  req.body.email,
      errors: errors.map(function (e) { return e.msg; })
    });
    return;
  }

  startSaw(req.body.email, req, res, function () {
    res.render('identity/email', {
      success: true,
      email:   req.body.email
    });
  }, function (sawError) {
    next(sawError);
  });
});

// Completion of a SAW request.
router.get('/callback', function (req, res, next) {
  console.log('BIG FANCY CONSOLE LOG!');
  if (!req.session.saw) {
    res.redirect(config.urls.homeUrl);
  }

  // Prepare the flash for errors.
  req.flash('error');

  completeSaw(req.user, req, res, function () {
    var referrer = req.flash('identity-add-referrer');
    res.redirect(referrer && referrer.length ? referrer[0] : config.urls.homeUrl);
  }, function (err) {
    req.flash('error', err);
    res.redirect('failure');
  });
});

// Handle failures.
router.get('/failure', function (req, res, next) {
  res.render('identity/error', {
    errors: req.flash('error')
  });
});

module.exports = {
  router: router,
  saw:    {
    start:    startSaw,
    complete: completeSaw
  }
};
