/**
 * Server application.
 * @module
 * @requires config
 */


/// --------------------------------------------------------------------------------------------------------------------
/// Server setup
/// --------------------------------------------------------------------------------------------------------------------

var assign           = require('lodash').assign,
    bodyParser       = require('body-parser'),
    cookieParser     = require('cookie-parser'),
    cors             = require('cors'),
    express          = require('express'),
    expressSession   = require('express-session'),
    expressValidator = require('express-validator'),
    favicon          = require('serve-favicon'),
    flash            = require('connect-flash'),
    logger           = require('morgan'),
    path             = require('path'),
    passport         = require('passport');

var config = require('./config');

var app = express();

// App settings
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger(config.logger.format, config.logger.options));

// Setup the body parsers and validator.
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(expressValidator());

// Setup sessions and the flash.
app.use(expressSession(config.session));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// Setup a handler to put global template data into res.locals.
app.use(function (req, res, next) {
  assign(res.locals, config.jadeLocals, {
    config:          config,
    isAuthenticated: req.isAuthenticated()
  });

  next();
});

/// --------------------------------------------------------------------------------------------------------------------
/// Setup routes.
/// --------------------------------------------------------------------------------------------------------------------

app.use('/api', cors({
  origin:      config.corsDomains,
  credentials: true
}));

// Setup the key management scheme routes.
var schemes = require('./schemes');
app.use('/api/public', schemes.public);
app.use('/api/private', schemes.private);
app.use('/api/params', schemes.params);

app.use('/api/whoami', require('./website/whoami'));

// Setup static resource routes.
app.use(express.static(path.join(__dirname, 'public')));

// Website routes that don't require authentication.
app.use('/', require('./website/auth'));
app.use('/', require('./website/index'));

// Ensure that the remaining routes are authenticated.
app.use(function (req, res, next) {
  if (req.isAuthenticated()) {
    next();
  } else {
    req.session.desiredUrl = req.originalUrl;
    res.redirect(config.urls.loginUrl);
  }
});

// Website routes with authentication.
app.use(config.urls.homeUrl, require('./website/account'));
app.use('/identity', require('./website/identity'));

/// --------------------------------------------------------------------------------------------------------------------
/// Setup error handling
/// --------------------------------------------------------------------------------------------------------------------

// Send a 404 for remaining routes.
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// Error handlers.
if (app.get('env') === 'development') {
  // Development handler.
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error:   err
    });
  });
} else {
  // Production handler.
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error:   {}
    });
  });
}

module.exports = app;
