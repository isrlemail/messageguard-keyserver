# MessageGuard File Server #

This is the file server for MessageGuard clients. It is designed to support encrypted attachments for MessageGuard. This code backs MessageGuard's primary file server, files.messageguard.io.

### What is this repository for? ###

This repository is maintained by the Internet Security Research Lab. We welcome researchers to fork this repo and improve on its security and/or features. We also welcome pull requests.

### How do I get set up? ###

1. Install Node JS.

2. Install the necessary packages.
```
#!bash
npm install
```

### Building the files. ###

Run the server using "npm start". Test the server using "npm test".

### Contribution guidelines ###
Make sure all of your files lint and follow the style defined in the idea style file.

### Who do I talk to? ###
For questions or comments contact Scott Ruoti <ruoti@isrl.byu.edu>.