/**
 * Web error.
 * @module
 */

var util = require('util');

/**
 * Error with a status code.
 * @param {number} status Status code.
 * @param {string} message Message describing the error.
 * @constructor
 */
function WebError(status, message) {
  Error.call(this);
  Error.captureStackTrace(this, arguments.callee);
  this.status = status;
  this.message = message;
}

util.inherits(WebError, Error);

module.exports = WebError;