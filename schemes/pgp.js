/**
 * This is the PGP key management scheme.
 * @module
 */

var express = require('express');

var PGPKey = require('../db').models.PGPKey;

var publicRouter  = express.Router(),
    privateRouter = express.Router();

/**
 * Get a PGP key from the database.
 * @param {boolean} isPublic Is this a public or a private key.
 * @returns {Function} Router function to get the key.
 */
var getKey = function (isPublic) {
  return function (req, res, next) {
    var where = {isPublic: isPublic};
    where[PGPKey.associations.Identity.foreignKey] = req.identity[PGPKey.associations.Identity.targetKey];

    PGPKey.findOne({where: where}).then(function (key) {
      if (key) {
        res.send(key.contents);
      } else {
        next();
      }
    }).catch(function (e) {
      res.status(500).send('Server error');
    });
  };
};

publicRouter.post('/', getKey(true));
privateRouter.post('/', getKey(false));

/**
 * Put a PGP key in the database.
 * @param {boolean} isPublic Is this a public or a private key.
 * @returns {Function} Router function to get the key.
 */
var putKey = function (isPublic) {
  return function (req, res, next) {
    if (!req.body.contents) {
      res.status(400).send('Missing key contents');
      return;
    }

    var where = {isPublic: isPublic};
    where[PGPKey.associations.Identity.foreignKey] = req.identity[PGPKey.associations.Identity.targetKey];

    PGPKey.findOne({where: where}).then(function (key) {
      if (key) {
        key.contents = req.body.contents;
        return key.save();
      } else {
        return PGPKey.createPGPKey(req.identity, {isPublic: isPublic, contents: req.body.contents});
      }
    }).then(function () {
      res.send();
    }).catch(function (e) {
      res.status(500).send('Server error');
    });
  };
};

publicRouter.put('/', putKey(true));
privateRouter.put('/', putKey(false));

/**
 * Delete a PGP key in the database.
 * @param {boolean} isPublic Is this a public or a private key.
 * @returns {Function} Router function to get the key.
 */
var deleteKey = function (isPublic) {
  return function (req, res, next) {
    var where = {isPublic: isPublic};
    where[PGPKey.associations.Identity.foreignKey] = req.identity[PGPKey.associations.Identity.targetKey];

    PGPKey.destroy({where: where}).then(function () {
      res.send();
    }).catch(function (e) {
      console.log(e);
      res.status(500).send('Server error');
    });
  };
};

publicRouter.delete('/', deleteKey(true));
privateRouter.delete('/', deleteKey(false));

module.exports = {
  public:  publicRouter,
  private: privateRouter
};