/**
 * This is the IBE key management scheme.
 * @module
 */

var express = require('express'),
    IBE     = require('ibejs');

var config = require('../config');

var privateRouter = express.Router(),
    paramsRouter  = express.Router();

// Set the server secret.
var ibe = new IBE(config.schemes.ibe.secret);
var publicParams = ibe.systemPublicKey;

// GET an IBE private key.
privateRouter.post('/', function (req, res, next) {
  var idString = JSON.stringify({
    type:  req.identity.type,
    value: req.identity.value
  }).toLowerCase();

  var keys = {
    publicParams: publicParams,
    privateKey:   ibe.getPrivateKey(idString)
  };

  res.send(keys);
});

paramsRouter.post('/', function (req, res, next) {
  res.send({publicParams: publicParams});
});

module.exports = {
  private: privateRouter,
  params:  paramsRouter
};
