/**
 * Module that loads all of the models.
 * @module
 */

var express    = require('express'),
    requireDir = require('require-dir'),
    util       = require('util');

var config   = require('../config'),
    Identity = require('../db').models.Identity;

var publicRouter  = express.Router(),
    privateRouter = express.Router(),
    paramsRouter  = express.Router();

[publicRouter, privateRouter, paramsRouter].forEach(function(router) {
  router.get('*', function (req, res, next) {
    res.status(400).send('GET not allowed at this endpoint');
  });
});

/**
 * Validate the identity portion of the request.
 */
var validateIdentity = function (req, res, next) {
  // Validate the request.
  req.checkBody('identity.type', 'identity.type is required').notEmpty();
  req.checkBody('identity.value', 'identity.value is required').notEmpty();
  var errors = req.validationErrors();
  if (errors) {
    res.status(400).send(util.inspect(errors));
    return;
  }

  // Fetch the identity.
  Identity.findIdentity(req.body.identity.type, req.body.identity.value).then(function (identity) {
    if (!identity) {
      res.status(404).send('Identity not found');
      return;
    } else {
      req.identity = identity;
      next();
    }
  }).catch(function (e) {
    res.status(500).send('Server error');
  });
};

/**
 * Validate that the user is authenticated and has access to the identity.
 */
var validateAuthenticated = function (req, res, next) {
  if (!req.isAuthenticated()) {
    res.status(401).send(config.urls.loginUrl);
    return;
  }

  if (req.identity[Identity.associations.User.foreignKey] !== req.user[Identity.associations.User.targetKey]) {
    res.status(403).send('The user does not have access to this identity for this operation');
    delete req.identity;
    return;
  }

  next();
};

// Protect the scheme routes based on identity ownership.
publicRouter.use(validateIdentity, function (req, res, next) {
  if (req.method === 'POST') {
    next();
  } else {
    validateAuthenticated(req, res, next);
  }
});

privateRouter.use(validateIdentity, validateAuthenticated);

// Register the schemes.
var hash = requireDir('.', {recurse: true});

Object.keys(hash).forEach(function (name) {
  var scheme = hash[name];

  if (scheme.public) {
    publicRouter.use('/' + name, scheme.public);
  }
  if (scheme.private) {
    privateRouter.use('/' + name, scheme.private);
  }
  if (scheme.params) {
    paramsRouter.use('/' + name, scheme.params);
  }
});

/**
 * Handler for 404.
 */
var notFound = function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
};

publicRouter.use(notFound);
privateRouter.use(notFound);
paramsRouter.use(notFound);

module.exports = {
  public:  publicRouter,
  private: privateRouter,
  params:  paramsRouter
};