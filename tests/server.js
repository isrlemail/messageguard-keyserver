/**
 * Create a server to be used for unit testing.
 * @module
 */

// Module dependencies.
var debug = require('debug')('messageguard-keyserver:test'),
    http  = require('http');

/**
 * Create the server.
 * @param port Port to listen on.
 */
var createServer = function (port) {
  // Create the server.
  var app = require('../app');
  app.set('port', port);

  var server = http.createServer(app);
  server.listen(port);

  // Setup listening handler.
  server.on('listening', function () {
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    debug('Listening on ' + bind);
  });

  // Setup error handling.
  server.on('error', function (error) {
    if (error.syscall !== 'listen') {
      throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
      case 'EACCES':
        console.error(bind + ' requires elevated privileges');
        process.exit(1);
        break;
      case 'EADDRINUSE':
        console.error(bind + ' is already in use');
        process.exit(1);
        break;
      default:
        throw error;
    }
  });

  return server;
};

module.exports = createServer;
