/**
 * Unit tests for the pgp scheme route.
 * @module
 * @requires tests/server
 */

var assert  = require('chai').assert,
    async   = require('async'),
    request = require('supertest');

var config   = require('../../config'),
    db       = require('../../db'),
    User     = db.models.User,
    server   = require('../server'),
    testUtil = require('../util');

describe('Testing the PGP scheme.', function () {

  var currentServer, publicKey, globalUser;

  beforeEach(function () {
    currentServer = server(3000);
    return db.sync({force: true}).then(testUtil.setupUser).then(function (user) {
      globalUser = user;
    });
  });

  afterEach(function (done) {
    currentServer.close(done);
  });

  it('Test getting an empty key', function testEmptyKey(done) {
    request(currentServer)
        .post('/public/pgp')
        .send({identity: {type: config.identities.messageGuardType, value: globalUser.username}})
        .expect(404)
        .end(function (err, res) {
          done(err);
        });
  });

  it('Test putting and retrieving a key', function testPutAndRetrieve(done) {
    testUtil.getAuthenticatedAgent(currentServer, function (err, agent) {
      if (err) {
        done(err);
        return;
      }

      async.series([
        function (cb) {
          agent
              .put('/public/pgp')
              .send(
                  {identity: {type: config.identities.messageGuardType, value: globalUser.username}, contents: 'test'})
              .expect(200)
              .end(function (err, res) {
                cb(err);
              });
        }, function (cb) {
          request(currentServer)
              .post('/public/pgp')
              .send({identity: {type: config.identities.messageGuardType, value: globalUser.username}})
              .expect(200)
              .end(function (err, res) {
                if (err) {
                  cb(err);
                  return;
                }

                assert.equal(res.text, 'test');
                cb();
              });
        }
      ], done);
    });
  });

  it('Test updating a key', function testPutAndRetrieve(done) {
    testUtil.getAuthenticatedAgent(currentServer, function (err, agent) {
      if (err) {
        done(err);
        return;
      }

      async.series([
        function (cb) {
          agent
              .put('/public/pgp')
              .send(
                  {identity: {type: config.identities.messageGuardType, value: globalUser.username}, contents: 'test'})
              .expect(200)
              .end(function (err, res) {
                cb(err);
              });
        }, function (cb) {
          agent
              .put('/public/pgp')
              .send(
                  {identity: {type: config.identities.messageGuardType, value: globalUser.username}, contents: 'test2'})
              .expect(200)
              .end(function (err, res) {
                cb(err);
              });
        }, function (cb) {
          request(currentServer)
              .post('/public/pgp')
              .send({identity: {type: config.identities.messageGuardType, value: globalUser.username}})
              .expect(200)
              .end(function (err, res) {
                if (err) {
                  cb(err);
                  return;
                }

                assert.equal(res.text, 'test2');
                cb();
              });
        }
      ], done);
    });
  });

  it('Test deleting a key', function testDelete(done) {
    testUtil.getAuthenticatedAgent(currentServer, function (err, agent) {
      if (err) {
        done(err);
        return;
      }

      async.series([
        function (cb) {
          agent
              .put('/public/pgp')
              .send(
                  {identity: {type: config.identities.messageGuardType, value: globalUser.username}, contents: 'test'})
              .expect(200)
              .end(function (err, res) {
                cb(err);
              });
        }, function (cb) {
          agent
              .delete('/public/pgp')
              .send({identity: {type: config.identities.messageGuardType, value: globalUser.username}})
              .expect(200)
              .end(function (err, res) {
                cb(err);
              });
        }, function (cb) {
          request(currentServer)
              .post('/public/pgp')
              .send({identity: {type: config.identities.messageGuardType, value: globalUser.username}})
              .expect(404)
              .end(function (err, res) {
                cb(err);
              });
        }
      ], done);
    });
  });

});