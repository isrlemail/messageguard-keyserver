/**
 * Unit tests for the ibe scheme route.
 * @module
 * @requires tests/server
 */

var assert  = require('chai').assert,
    async   = require('async'),
    IBE     = require('ibejs'),
    request = require('supertest');

var config   = require('../../config'),
    db       = require('../../db'),
    server   = require('../server'),
    testUtil = require('../util');

describe('Testing the IBE scheme.', function () {
  // IBE is very slow; upping the timeout to avoid false negatives.
  this.timeout(5000);

  var currentServer, ibe, publicKey, globalUser;

  beforeEach(function () {
    ibe = new IBE(config.schemes.ibe.secret);
    publicKey = ibe.systemPublicKey;

    currentServer = server(3000);
    return db.sync({force: true}).then(testUtil.setupUser).then(function (user) {
      globalUser = user;
    });
  });

  afterEach(function (done) {
    currentServer.close(done);
  });

  it('Test that we are getting the public key', function testPublicKey(done) {
    request(currentServer)
        .post('/api/params/ibe')
        .expect(200)
        .end(function (err, res) {
          if (err) {
            done(err);
            return;
          }

          assert.deepEqual(res.body.publicKey, publicKey);
          done();
        });
  });

  it('Test that we get a valid private key', function testPrivateKey(done) {
    testUtil.getAuthenticatedAgent(currentServer, function (err, agent) {
      if (err) {
        done(err);
        return;
      }

      agent
          .post('/api/private/ibe')
          .send({identity: {type: config.identities.messageGuardType, value: globalUser.username}})
          .expect(200)
          .end(function (err, res) {
            if (err) {
              done(err);
              return;
            }

            var userId = JSON.stringify(
                {type: config.identities.messageGuardType, value: globalUser.username}).toLowerCase();
            var encryptedMessage = ibe.encrypt(userId, 'message');
            var message = ibe.decrypt(JSON.parse(res.text), encryptedMessage);
            assert.equal(message, 'message');
            done();
          });
    });
  });

  it('Test ibe ignores case', function testCasing(done) {
    testUtil.getAuthenticatedAgent(currentServer, function (err, agent) {
      if (err) {
        done(err);
        return;
      }

      var privateKey;

      async.series([
        function (cb) {
          agent
              .post('/api/private/ibe')
              .send({
                identity: {
                  type: config.identities.messageGuardType.toUpperCase(), value: globalUser.username.toUpperCase()
                }
              })
              .expect(200)
              .end(function (err, res) {
                if (err) {
                  cb(err);
                  return;
                }

                privateKey = JSON.parse(res.text);
                cb();
              });
        }, function (cb) {
          agent
              .post('/api/private/ibe')
              .send({
                identity: {
                  type: config.identities.messageGuardType.toLowerCase(), value: globalUser.username.toLowerCase()
                }
              })
              .expect(200)
              .end(function (err, res) {
                if (err) {
                  cb(err);
                  return;
                }

                assert.deepEqual(JSON.parse(res.text), privateKey);
                cb();
              });
        }
      ], done);
    });
  });

});