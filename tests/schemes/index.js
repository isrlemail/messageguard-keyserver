/**
 * Unit tests for the the scheme routes.
 * @module
 * @requires tests/server
 */

var assert  = require('chai').assert,
    async   = require('async'),
    request = require('supertest');

var config   = require('../../config'),
    db       = require('../../db'),
    User     = db.models.User,
    server   = require('../server'),
    testUtil = require('../util');

describe('Testing the scheme authentication.', function () {

  var currentServer, globalUser;

  beforeEach(function () {
    currentServer = server(3000);
    return db.sync({force: true}).then(testUtil.setupUser).then(function (user) {
      globalUser = user;
    });
  });

  afterEach(function (done) {
    currentServer.close(done);
  });

  it('Test identity parsing', function testIdentityParsing(done) {
    async.parallel([
      function (cb) {
        request(currentServer)
            .post('/api/public')
            .expect(400)
            .end(function (err, res) {
              cb(err);
            });
      }, function (cb) {
        request(currentServer)
            .post('/api/public')
            .send({identity: {type: config.identities.messageGuardType}})
            .expect(400)
            .end(function (err, res) {
              cb(err);
            });
      }, function (cb) {
        request(currentServer)
            .post('/api/public')
            .send({identity: {value: globalUser.username}})
            .expect(400)
            .end(function (err, res) {
              cb(err);
            });
      }, function (cb) {
        request(currentServer)
            .post('/api/public')
            .send({identity: {type: config.identities.messageGuardType, value: globalUser.username}})
            .expect(404)
            .end(function (err, res) {
              cb(err);
            });
      }
    ], done);
  });

  it('Test identity lookup', function testIdentityLookup(done) {
    async.parallel([
      function (cb) {
        request(currentServer)
            .post('/api/public')
            .send({identity: {type: config.identities.messageGuardType, value: globalUser.username}})
            .expect(404)
            .end(function (err, res) {
              if (err) {
                cb(err);
                return;
              }

              assert.notEqual(res.text, 'Identity not found');
              cb();
            });
      }, function (cb) {
        request(currentServer)
            .post('/api/public')
            .send({
              identity: {
                type: config.identities.messageGuardType.toUpperCase(), value: globalUser.username.toUpperCase()
              }
            })
            .expect(404)
            .end(function (err, res) {
              if (err) {
                cb(err);
                return;
              }

              assert.notEqual(res.text, 'Identity not found');
              cb();
            });
      }, function (cb) {
        request(currentServer)
            .post('/api/public')
            .send({
              identity: {
                type: config.identities.messageGuardType.toLowerCase(), value: globalUser.username.toLowerCase()
              }
            })
            .expect(404)
            .end(function (err, res) {
              if (err) {
                cb(err);
                return;
              }

              assert.notEqual(res.text, 'Identity not found');
              cb();
            });
      }, function (cb) {
        request(currentServer)
            .post('/api/public')
            .send({identity: {type: config.identities.messageGuardType, value: 'badName'}})
            .expect(404)
            .end(function (err, res) {
              if (err) {
                cb(err);
                return;
              }

              assert.equal(res.text, 'Identity not found');
              cb();
            });
      }
    ], done);
  });

  it('Test authorization to identity', function testIdentityAuthorization(done) {
    testUtil.getAuthenticatedAgent(currentServer, function (err, agent) {
      if (err) {
        done(err);
        return;
      }

      async.parallel([
        function (cb) {
          request(currentServer)
              .post('/api/private')
              .send({identity: {type: config.identities.messageGuardType, value: globalUser.username}})
              .expect(401)
              .end(function (err, res) {
                if (err) {
                  cb(err);
                  return;
                }

                assert.equal(res.text, config.urls.loginUrl);
                cb();
              });
        }, function (cb) {
          agent
              .post('/api/private')
              .send({identity: {type: config.identities.messageGuardType, value: globalUser.username}})
              .expect(404)
              .end(function (err, res) {
                if (err) {
                  cb(err);
                  return;
                }

                cb();
              });
        }, function (cb) {
          User.createWithPassword('test2', 'password').then(function (user) {
            agent
                .post('/api/private')
                .send({identity: {type: config.identities.messageGuardType, value: user.username}})
                .expect(403)
                .end(function (err, res) {
                  if (err) {
                    cb(err);
                    return;
                  }

                  cb();
                });
          });
        }
      ], done);
    });
  });

  it("Test that /public POST and /params don't need authorization", function testAuthenticationNotNeeded(done) {
    async.parallel([
      function (cb) {
        request(currentServer)
            .post('/api/public')
            .send({identity: {type: config.identities.messageGuardType, value: globalUser.username}})
            .expect(404)
            .end(function (err, res) {
              if (err) {
                cb(err);
                return;
              }

              cb();
            });
      }, function (cb) {
        request(currentServer)
            .post('/api/params')
            .expect(404)
            .end(function (err, res) {
              if (err) {
                cb(err);
                return;
              }

              cb();
            });
      }
    ], done);
  });

  it("Test that GET is disallowed", function testGETDisabled(done) {
    async.parallel([
      function (cb) {
        request(currentServer)
            .get('/api/public')
            .expect(400)
            .end(function (err, res) {
              if (err) {
                cb(err);
                return;
              }

              assert.equal(res.text, 'GET not allowed at this endpoint');
              cb();
            });
      }, function (cb) {
        request(currentServer)
            .get('/api/private')
            .expect(400)
            .end(function (err, res) {
              if (err) {
                cb(err);
                return;
              }

              assert.equal(res.text, 'GET not allowed at this endpoint');
              cb();
            });
      }, function (cb) {
        request(currentServer)
            .get('/api/params')
            .expect(400)
            .end(function (err, res) {
              if (err) {
                cb(err);
                return;
              }

              assert.equal('GET not allowed at this endpoint', res.text);
              cb();
            });
      }
    ], done);
  });


});