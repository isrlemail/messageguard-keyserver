/**
 * Util functions for the test.
 */

var request = require('supertest');

var db   = require('../db'),
    User = db.models.User;

module.exports = {

  /**
   * Gets a promise that will setup the user.
   * @returns {Promise}
   */
  setupUser: function () {
    return User.createWithPassword('test', 'password');
  },

  /**
   * Get an authenticated request object.
   * @param {http} HTTP server.
   * @param {Function} done Callback that will be passed the agent.
   */
  getAuthenticatedAgent: function (app, done) {
    var agent = request.agent(app);
    agent
        .post('/login')
        .send({'username': 'test', password: 'password'})
        .expect(302)
        .end(function (err, res) {
          if (err) {
            done(err);
            return;
          }
          done(null, agent);
        });
  }

};
