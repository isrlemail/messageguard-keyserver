/**
 * Unit test for the Identity DB model.
 * @module
 */

var assert = require('chai').assert;

var config   = require('../../config'),
    db       = require('../../db'),
    User     = db.models.User,
    Identity = db.models.Identity;

describe('Testing identity db model', function () {

  var globalUser;

  // Reset the database.
  beforeEach(function () {
    return db.sync({force: true}).then(function () {
      return User.createWithPassword('test', 'password');
    }).then(function (user) {
      globalUser = user;
    });
  });

  it('Test creating an identity', function testCreate() {
    return Identity.createIdentity(globalUser,
        {type: 'test', value: 'val', displayName: 'testing', data: 'Data!'}).then(function (identity) {

      assert.equal(identity.type, 'test');
      assert.equal(identity.value, 'val');
      assert.equal(identity.displayName, 'testing');
      assert.equal(identity.data, 'Data!');

      return identity.getUser();
    }).then(function (user) {
      assert.isTrue(globalUser.equals(user));
    });
  });

  it('Test null values not allowed.', function testNullNotAllowed() {
    return Identity.create({type: null, value: 'value', displayName: 'displayName'}).then(function () {
      assert.equal('No nulls allowed', 'Null allowed');
    }).catch(db.ValidationError, function () {
    }).then(function () {
      return Identity.create({type: 'type', value: null, displayName: 'displayName'}); // Should throw an exception.
    }).then(function () {
      assert.equal('No nulls allowed', 'Null allowed');
    }).then(function () {
      return Identity.create({type: 'type', value: 'value', displayName: null}); // Should throw an exception.
    }).then(function () {
      assert.equal('No nulls allowed', 'Null allowed');
    }).catch(db.ValidationError, function () {
    });
  });

  it('Test that identities are automatically created.', function testAutoCreateIdentity() {
    var createdUser;

    return User.createWithPassword('test2', 'password').then(function (user) {
      createdUser = user;
      return Identity.findIdentity(config.identities.messageGuardType, user.username);
    }).then(function (identity) {
      assert.isNotNull(identity);
      assert.equal(identity.type, config.identities.messageGuardType);
      assert.equal(identity.value, createdUser.username);
      assert.equal(identity.displayName, createdUser.username);
      assert.isNull(identity.data);

      return identity.getUser();
    }).then(function (user) {
      assert.isTrue(createdUser.equals(user));
    });
  });

  it('Test Finding an identity.', function testFind() {
    var createdUser, createdIdentity;

    return User.createWithPassword('test2', 'password').then(function (user) {
      createdUser = user;
      return Identity.createIdentity(createdUser, {type: 'type', value: 'value', displayName: 'displayName'});
    }).then(function (identity) {
      createdIdentity = identity;
      return Identity.findIdentity('type', 'value');
    }).then(function (identity) {
      assert.isTrue(createdIdentity.equals(identity));

      return Identity.findIdentity('type2', 'value');
    }).then(function (identity) {
      assert.isNull(identity);
    });
  });

  it('Test that duplicate names are rejected', function testDuplicates() {
    return Identity.createIdentity(globalUser,
        {type: 'type', value: 'value', displayName: 'displayName'}).then(function (user) {
      return Identity.createIdentity(globalUser,
          {type: 'type', value: 'value', displayName: 'displayName'}); // Should throw an exception.
    }).then(function () {
      assert.equal('No UniqueContratraintError', 'UniqueConstraintError');
    }).catch(db.UniqueConstraintError, function () {
    }).then(function () {
      return Identity.createIdentity(globalUser,
          {type: 'Type', value: 'value', displayName: 'displayName'}); // Should throw an exception.
    }).then(function () {
      assert.equal('No UniqueContratraintError', 'UniqueConstraintError');
    }).catch(db.UniqueConstraintError, function () {
    }).then(function () {
      return Identity.createIdentity(globalUser,
          {type: 'type2', value: 'value', displayName: 'displayName'}); // Should not throw an exception.
    });
  });

});
