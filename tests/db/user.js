/**
 * Unit test for the User DB model.
 * @module
 */

var assert = require('chai').assert;

var db   = require('../../db'),
    User = db.models.User;

describe('Testing user db model', function () {

  // Reset the database.
  beforeEach(function () {
    return db.sync({force: true});
  });

  it('Test creating a user with password', function testCreateWithPassword() {
    return User.createWithPassword('test', 'password').then(function (user) {
      assert.equal(user.username, 'test');
      assert.notEqual(user.password, 'password');
    });
  });

  it('Test null values not allowed.', function testNullNotAllowed() {
    return User.create({username: null, password: 'test'}).then(function () {
      assert.equal('No nulls allowed', 'Null allowed');
    }).catch(db.ValidationError, function () {
    }).then(function () {
      return User.create({username: 'username', password: null}); // Should throw an exception.
    }).then(function () {
      assert.equal('No nulls allowed', 'Null allowed');
    }).catch(db.ValidationError, function () {
    });
  });

  it('Test that duplicate names are rejected', function testDuplicates() {
    return User.createWithPassword('test', 'password').then(function (user) {
      assert.equal(user.username, 'test');
      assert.notEqual(user.password, 'password');
    }).then(function () {
      return User.createWithPassword('test', 'password'); // Should throw an exception.
    }).then(function () {
      assert.equal('No UniqueContratraintError', 'UniqueConstraintError');
    }).catch(db.UniqueConstraintError, function () {
    }).then(function () {
      return User.createWithPassword('Test', 'password'); // Should throw an exception.
    }).then(function () {
      assert.equal('No UniqueContratraintError', 'UniqueConstraintError');
    }).catch(db.UniqueConstraintError, function () {
    }).then(function () {
      return User.createWithPassword('Test2', 'password'); // Should not throw an exception.
    });
  });

  it('Test finding a user with password', function testFind(done) {
    var user1, user2;

    return User.createWithPassword('test', 'password').then(function (user) {
      user1 = user;
      return User.createWithPassword('test2', 'password2');
    }).then(function (user) {
      user2 = user;
      return User.findWithPassword('test', 'password');
    }).then (function (user) {
      assert.ok(user.equals(user1));
      return User.findWithPassword('test2', 'password2');
    }).then (function (user) {
      assert.ok(user.equals(user2));
      return User.findWithPassword('test2', 'password');
    }).then (function () {
      assert.equal('No incorrect password error', 'Incorrect password error');
    }).catch (function (e) {
      assert.equal(e.message, 'Incorrect password');
    }).then(function () { done(); }, function (e) { done(e); });
  });

});
