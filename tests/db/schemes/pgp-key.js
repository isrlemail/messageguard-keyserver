/**
 * Unit test for the PGPKey DB model.
 * @module
 */

var assert = require('chai').assert;

var config   = require('../../../config'),
    db       = require('../../../db'),
    User     = db.models.User,
    Identity = db.models.Identity,
    PGPKey   = db.models.PGPKey;

describe('Testing pgp-key db model', function () {

  var globalUser, globalIdentity;

  // Reset the database.
  beforeEach(function () {
    return db.sync({force: true}).then(function () {
      return User.createWithPassword('test', 'password');
    }).then(function (user) {
      globalUser = user;
      return Identity.findIdentity(config.identities.messageGuardType, user.username);
    }).then (function (identity) {
      globalIdentity = identity;
    });
  });

  it('Test creating a PGP key', function testCreate() {
    return PGPKey.createPGPKey(globalIdentity, {isPublic: true, contents: 'test'}).then(function (key) {
      assert.equal(key.isPublic, true);
      assert.equal(key.contents, 'test');

      return key.getIdentity();
    }).then(function (identity) {
      assert.isTrue(globalIdentity.equals(identity));
    });
  });

  it('Test null values not allowed.', function testNullNotAllowed() {
    return PGPKey.createPGPKey(globalIdentity, {isPublic: null, contents: 'test'}).then(function () {
      assert.equal('No nulls allowed', 'Null allowed');
    }).catch(db.ValidationError, function () {
    }).then(function () {
      return PGPKey.createPGPKey(globalIdentity, {isPublic: true, contents: null}); // Should throw an exception.
    }).then(function () {
      assert.equal('No nulls allowed', 'Null allowed');
    }).catch(db.ValidationError, function () {
    });
  });

  it('Test Finding an PGP key.', function testFind() {
    var createdUser, createdIdentity, createdKey;

    return User.createWithPassword('test2', 'password').then(function (user) {
      createdUser = user;
      return Identity.createIdentity(createdUser, {type: 'type', value: 'value', displayName: 'displayName'});
    }).then(function (identity) {
      createdIdentity = identity;
      return PGPKey.createPGPKey(createdIdentity, {isPublic: false, contents: 'test'});
    }).then(function (key) {
      createdKey = key;

      var where = {isPublic: false};
      where[PGPKey.associations.Identity.foreignKey] = createdIdentity[PGPKey.associations.Identity.targetKey];
      return PGPKey.findOne(where);
    }).then(function (key) {
      assert.isTrue(createdKey.equals(key));

      var where = {isPublic: true};
      where[PGPKey.associations.Identity.foreignKey] = createdIdentity[PGPKey.associations.Identity.targetKey];
      return PGPKey.findOne({where: where});
    }).then(function (key) {
      assert.isNull(key);
    });
  });

  it('Test that duplicate names are rejected', function testDuplicates() {
    return PGPKey.createPGPKey(globalIdentity, {isPublic: false, contents: 'test'}).then(function () {
      return PGPKey.createPGPKey(globalIdentity, {isPublic: false, contents: 'test'}); // Should throw an exception.
    }).then(function () {
      assert.equal('No UniqueContratraintError', 'UniqueConstraintError');
    }).catch(db.UniqueConstraintError, function () {
    }).then(function () {
      return PGPKey.createPGPKey(globalIdentity, {isPublic: true, contents: 'test'}); // Should not throw an exception.
    });
  });

});
